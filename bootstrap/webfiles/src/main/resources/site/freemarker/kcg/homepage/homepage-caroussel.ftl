<#include "../../include/imports.ftl">
<#--
  Copyright 2014 Hippo B.V. (http://www.onehippo.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<#-- @ftlvariable name="item" type="com.incentro.dmcr.beans.Banner" -->
<#-- @ftlvariable name="pageable" type="org.onehippo.cms7.essentials.components.paging.Pageable" -->
<#-- @ftlvariable name="cparam" type="org.onehippo.cms7.essentials.components.info.EssentialsCarouselComponentInfo" -->
<@hst.headContribution category="htmlHead">
<link rel="stylesheet" href="<@hst.webfile  path="/css/slider.css"/>" type="text/css"/>
</@hst.headContribution>

<#if pageable?? && pageable.items?has_content>
<div id="slider">
      <div id="slider-stage">
          <ul id="slider-list">
           	<#list pageable.items as item>
                <#if item_index==0>
                    <#assign active = ' active'/>
                <#else>
                    <#assign active = ''/>
                </#if>
                <a href="<@hst.link hippobean=item.link />">
                	<li class="s1 item${active}" style="background:url('<@hst.link hippobean=item.image />') top left no-repeat;">                    
                	    <div>
                            <div>
                	            <@hst.html hippohtml=item.content/>
                            </div>
                	    </div>
                	</li>
                </a>
            </#list>
            </ul>        
      </div>

      <div class="arrow" id="previous"></div>
      <div class="arrow" id="next"></div>
    </div>
    
    <@hst.headContribution category="htmlBodyEnd">
    <script type="text/javascript" src="<@hst.webfile path="/js/jquery-1.11.2.min.js"/>"></script>
    </@hst.headContribution>
    <@hst.headContribution category="htmlBodyEnd">
    <script type="text/javascript" src="<@hst.webfile path="/js/bootstrap.min.js"/>"></script>
    </@hst.headContribution>
    <@hst.headContribution category="htmlBodyEnd">
    <script src="<@hst.webfile path="/js/slider.js"/>" type="text/javascript"></script>
    </@hst.headContribution>    
<#elseif editMode>
  <img src="<@hst.link path='/images/essentials/catalog-component-icons/carousel.png'/>"> Click to edit Carousel
</#if>
