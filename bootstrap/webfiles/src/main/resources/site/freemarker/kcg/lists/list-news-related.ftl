<#include "../../include/imports.ftl">
<@hst.setBundle basename="kcg.news"/>

<h2 class="left-title"><@fmt.message key="kcg.news.related.title" var="newsRelatedTitle"/>${newsRelatedTitle?html}</h2>
<#if document.relatednews?has_content>
<ul id="submenu">
    <#list document.relatednews as item>
        <@hst.link var="link" hippobean=item />
        <li class="link"><a href="${link}">${item.title}</a></li>
    </#list>
</ul>
<#else>
    <@fmt.message key="kcg.news.related.nothing" var="newsRelatedNothing"/>${newsRelatedNothing?html}
</#if>

