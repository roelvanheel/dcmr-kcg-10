<!doctype html>
<#--
  Copyright 2014 Hippo B.V. (http://www.onehippo.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
    <#include "../include/imports.ftl">
<div id="header-wrap">
	<div id="header">
		<div id="topbar">
			<div id="top-menu">
				<@hst.include ref="topmenu"/>
			</div>
		
			<!-- AddThis Button BEGIN -->
	      	<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=xa-4e2d45107f58fded"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>
	       	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e2d45107f58fded"></script>
	    	<!-- AddThis Button END -->

			<@hst.setBundle basename="essentials.searchbox"/>
			<form class="navbar-form" role="search" action="<@hst.link siteMapItemRefId="search" />" method="get">
    			<div class="input-group">
        			<input type="text" class="form-control" placeholder="<@fmt.message key='searchbox.placeholder'/>" name="query">
        			<div class="input-group-btn">
			            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        			</div>
    			</div>
			</form>
		
		</div>
		<div class="reset"></div>    	
	    <h1 id="logo"><a href="#">Kenniscentrum <b>Geluid</b> Rijnmond</a></h1>
	    	
		<@hst.include ref="mainmenu"/>
    	<div class="reset"></div>

    	<div id="breadcrumbs">
			<@hst.include ref="breadcrumbmenu"/>
    	</div><!-- eof:#breadcrumbs -->  
    </div>
</div>