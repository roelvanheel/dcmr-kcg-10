<#include "../../include/imports.ftl">
<@hst.setBundle basename="kcg.projects"/>

<h2 class="left-title"><@fmt.message key="kcg.projects.related.title" var="projectsRelatedTitle"/>${projectsRelatedTitle?html}</h2>
<#if document.relatedprojects?has_content>
<ul id="submenu">
    <#list document.relatedprojects as item>
        <@hst.link var="link" hippobean=item />
        <li class="link"><a href="${link}">${item.title}</a></li>
    </#list>
</ul>
<#else>
    <@fmt.message key="kcg.projects.related.nothing" var="projectsRelatedNothing"/>${projectsRelatedNothing?html}
</#if>

