<#include "../../include/imports.ftl">
<#--
  Copyright 2014 Hippo B.V. (http://www.onehippo.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<#-- @ftlvariable name="item" type="com.incentro.dmcr.beans.ProjectDocument" -->
<#-- @ftlvariable name="pageable" type="org.onehippo.cms7.essentials.components.paging.Pageable" -->
<@hst.setBundle basename="kcg.projects"/>
<#if pageable?? && pageable.items?has_content>
	<#list pageable.items as item>
		<@hst.link var="link" hippobean=item />
    <div class="article-project">
        <article class="has-edit-button">
			<@hst.cmseditlink hippobean=item/>
			<#if item.image.overview??>
                <img src="<@hst.link hippobean=item.image.overview />" alt="${item.title}"/>
			</#if>
            <h2><a href="${link}">${item.title}</a></h2>

			<#if item.date?? && item.date.time??>
                <p><@fmt.formatDate value=item.date.time type="both" dateStyle="medium" timeStyle="short"/></p>
			</#if>
		<#--
                <#if item.author??>
                        <p>${item.author}</p>
                    </#if>
                    <#if item.location??>
                        <p>${item.location}</p>
                    </#if>
                    <#if item.source??>
                        <p>${item.source}</p>
                    </#if>
                    -->
			<#if item.introduction??>
                <p>${item.introduction}</p>
			</#if>
            <a class="read-more" href="${link}"><@fmt.message key="kcg.projects.readmore" var="linkreadmore"/>${linkreadmore?html} &gt;&gt;</a>
        </article>
    </div>
	</#list>
	<#if pageable.showPagination??>
		<#include "../../include/pagination.ftl">
	</#if>
<#-- @ftlvariable id="editMode" type="java.lang.Boolean"-->
<#elseif editMode>
<img src="<@hst.link path='/images/essentials/catalog-component-icons/news-list.png'/>"> Click to edit News List
</#if>


