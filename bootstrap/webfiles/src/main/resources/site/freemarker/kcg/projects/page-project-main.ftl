<#include "../../include/imports.ftl">
<#--
  Copyright 2014 Hippo B.V. (http://www.onehippo.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<#-- @ftlvariable name="document" type="com.incentro.dmcr.beans.ProjectDocument" -->
<#if document??>
    <@hst.link var="link" hippobean=document/>
<div class="article-project">
    <article class="has-edit-button">
        <@hst.cmseditlink hippobean=document/>
        <h3><a href="${link}">${document.title}</a></h3>

        <div class="project-details">
            <#if document.date??>
                <span><@fmt.formatDate value=document.date.time type="both" dateStyle="medium" timeStyle="short"/></span>
                <span>&nbsp;|&nbsp;</span>
            </#if>
            <#if document.endDate??>
                <span><@fmt.formatDate value=document.endDate.time type="both" dateStyle="medium" timeStyle="short"/></span>
                <span>&nbsp;|&nbsp;</span>
            </#if>
            <#if document.author??>
                <span>${document.author}</span>
                <span>&nbsp;|&nbsp;</span>
            </#if>
            <#if document.location??>
                <span>${document.location}</span>
                <span>&nbsp;|&nbsp;</span>
            </#if>
        </div>
        <@hst.html hippohtml=document.content/>
    </article>
</div>
</#if>