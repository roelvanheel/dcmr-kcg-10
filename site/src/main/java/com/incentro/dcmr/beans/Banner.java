package com.incentro.dcmr.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "kcg:bannerdocument")
@Node(jcrType = "kcg:bannerdocument")
public class Banner extends BaseDocument {
    private final static String TITLE = "kcg:title";
    private final static String CONTENT = "kcg:content";
    private final static String LINK = "kcg:link";
    private final static String IMAGE = "kcg:image";

    @HippoEssentialsGenerated(internalName = "kcg:title")
    public String getTitle() {
        return getProperty(TITLE);
    }

    @HippoEssentialsGenerated(internalName = "kcg:content")
    public HippoHtml getContent() {
        return getHippoHtml(CONTENT);
    }

    @HippoEssentialsGenerated(internalName = "kcg:link")
    public HippoBean getLink() {
        return getLinkedBean(LINK, HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "kcg:image")
    public Kcg getImage() {
        return getLinkedBean(IMAGE, Kcg.class);
    }
}
