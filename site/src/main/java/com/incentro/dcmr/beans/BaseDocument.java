package com.incentro.dcmr.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="kcg:basedocument")
public class BaseDocument extends HippoDocument {

}
