package com.incentro.dcmr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;

@HippoEssentialsGenerated(internalName = "kcg:kcg")
@Node(jcrType = "kcg:kcg")
public class Kcg extends HippoGalleryImageSet {
	@HippoEssentialsGenerated(internalName = "kcg:banner")
	public HippoGalleryImageBean getBanner() {
		return getBean("kcg:banner", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:overview")
	public HippoGalleryImageBean getOverview() {
		return getBean("kcg:overview", HippoGalleryImageBean.class);
	}
}
