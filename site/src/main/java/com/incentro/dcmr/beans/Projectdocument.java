package com.incentro.dcmr.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import com.incentro.dcmr.beans.Kcg;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;

@HippoEssentialsGenerated(internalName = "kcg:projectdocument")
@Node(jcrType = "kcg:projectdocument")
public class Projectdocument extends BaseDocument {
	@HippoEssentialsGenerated(internalName = "kcg:title")
	public String getTitle() {
		return getProperty("kcg:title");
	}

	@HippoEssentialsGenerated(internalName = "kcg:introduction")
	public String getIntroduction() {
		return getProperty("kcg:introduction");
	}

	@HippoEssentialsGenerated(internalName = "kcg:date")
	public Calendar getDate() {
		return getProperty("kcg:date");
	}

	@HippoEssentialsGenerated(internalName = "kcg:author")
	public String getAuthor() {
		return getProperty("kcg:author");
	}

	@HippoEssentialsGenerated(internalName = "kcg:source")
	public String getSource() {
		return getProperty("kcg:source");
	}

	@HippoEssentialsGenerated(internalName = "kcg:location")
	public String getLocation() {
		return getProperty("kcg:location");
	}

	@HippoEssentialsGenerated(internalName = "kcg:content")
	public HippoHtml getContent() {
		return getHippoHtml("kcg:content");
	}

	@HippoEssentialsGenerated(internalName = "kcg:image")
	public Kcg getImage() {
		return getLinkedBean("kcg:image", Kcg.class);
	}

	@HippoEssentialsGenerated(internalName = "kcg:relatedprojects")
	public List<HippoBean> getRelatedprojects() {
		return getLinkedBeans("kcg:relatedprojects", HippoBean.class);
	}
}
